package entity;

import java.util.Date;

/**
 * Created by Administrator on 22/02/2016.
 */
public class Vol  extends Entity{
    private Date depart;
    private float prix;

    public Date getDepart() {
        return depart;
    }

    public void setDepart(Date depart) {
        this.depart = depart;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}
