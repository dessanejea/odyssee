package entity;

import repository.Doctrine;

/**
 * Created by Administrator on 22/02/2016.
 */
public class Sejour extends Entity{
    protected int tauxRecommendation;
    protected String type;
    protected String idLieu;
    protected String nom;

    public int getTauxRecommendation() {
        return tauxRecommendation;
    }

    public void setTauxRecommendation(int tauxRecommendation) {
        this.tauxRecommendation = tauxRecommendation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Lieu getLieu() {
        return (Lieu) new Doctrine().getLieuRepository().getOneById(idLieu);
    }

    public void setLieu(Lieu lieu) {
        this.idLieu = lieu.getId();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public static void recherche(String type, String destination){

    }

    public static void estimationDuCout(){

    }

}
