package entity;

import java.util.Date;

/**
 * Created by Administrator on 22/02/2016.
 */
public class Souhait extends Entity{
    private String sejour;
    private Date expire;

    public String getSejour() {
        return sejour;
    }

    public void setSejour(String sejour) {
        this.sejour = sejour;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }
}
