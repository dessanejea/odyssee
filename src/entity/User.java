package entity;

/**
 * Created by Administrator on 11/03/2016.
 */

public class User extends Entity{

    private String lastName;

    private String firstName;

    private String mail;

    private String password;


    private String role;
/*
    @Override
    public String toString(){

        return "nom : " +firstName + "\nprenom : " + lastName + "\nrole : " + role;
    }
*/
    public User() {
        this("inconnu", "inconnu", "ROLE_USER");
    }

    /**
     * @param lastName
     * @param firstName
     * @param role
     */
    public User(String lastName, String firstName, String role) {
        super();
        this.lastName = lastName;
        this.firstName = firstName;
        this.role = role;
    }

    /**
     * @param lastName
     * @param firstName
     * @param role
     * @param mail
     * @param password
     */
    public User(String lastName, String firstName, String role,String mail, String password) {
        this(lastName,firstName,role);
        this.mail = mail;
        this.password = password;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
