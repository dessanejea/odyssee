package entity;

/**
 * Created by Administrator on 22/02/2016.
 */
public class Partenaire  extends Entity{
    private String nom;
    private int reduction;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getReduction() {
        return reduction;
    }

    public void setReduction(int reduction) {
        this.reduction = reduction;
    }
}
