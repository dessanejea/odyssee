package entity;

import repository.Doctrine;
import repository.Repository;
import repository.UserRepository;

import java.io.*;
import java.util.UUID;

import repository.*;

import com.google.gson.*;

/**
 * Created by Administrator on 12/03/2016.
 */
public class Entity implements Serializable{

    protected String id;

    public Entity(){
        this.id = UUID.randomUUID().toString();
    }


    public String getEntityClass(){
        return this.getClass().toString().replace("class entity.","");
    }



    public void persist(){

        Doctrine doctrine = new Doctrine();
        doctrine.setData(this.getEntityClass(),this);

    }

    public String getId(){
        return this.id;
    }

}
