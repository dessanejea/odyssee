package entity;

import java.util.Date;

/**
 * Created by Administrator on 22/02/2016.
 */
public class Voiture extends Entity{
    private float prixAuKm;
    private Date depart;
    private float prixALH;

    public float getPrixAuKm() {
        return prixAuKm;
    }

    public void setPrixAuKm(float prixAuKm) {
        this.prixAuKm = prixAuKm;
    }

    public Date getDepart() {
        return depart;
    }

    public void setDepart(Date depart) {
        this.depart = depart;
    }

    public float getPrixALH() {
        return prixALH;
    }

    public void setPrixALH(float prixALH) {
        this.prixALH = prixALH;
    }
}
