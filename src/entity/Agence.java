package entity;

import java.util.Vector;

/**
 * Created by Administrator on 22/02/2016.
 */
public class Agence extends Entity{
    private String lieu;
    private String responsable;
    private String adresse;
    private Vector<String> liste_clients;

    private Vector<String> liste_partenaires;

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void getClient() {
        this.liste_clients = liste_clients;
    }

    public void addClient(String liste_clients) {
        this.liste_clients.add(liste_clients);
    }

    public void getPartenaire() {
        this.liste_partenaires = liste_partenaires;
    }

    public void setPartenaire(String liste_partenaires) {
        this.liste_partenaires.add(liste_partenaires);
    }

    public void addClient(){

    }

    public void addPartenaire(){

    }

}
