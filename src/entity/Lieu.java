package entity;

/**
 * Created by Administrator on 26/04/2016.
 */
public class Lieu extends Entity {

    private String pays;

    private String ville;


    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
