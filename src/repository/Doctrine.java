package repository;

import com.google.gson.reflect.TypeToken;
import entity.*;

import java.io.*;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import com.google.gson.*;


/**
 * Created by Administrator on 12/03/2016.
 */
public class Doctrine {



    public LieuRepository getLieuRepository(){
        return new LieuRepository();
    }


    public UserRepository getUserRepository(){
        return new UserRepository();
    }


    public HashMap<String, Entity> getData(String type){

        HashMap<String, Entity> data = new HashMap<String, Entity>();
        Type dataType;
        switch (type){
            case "Agence":
                dataType =  new TypeToken<HashMap<String, Agence>>(){}.getType();
                break;
            case "Autobus":
                dataType =  new TypeToken<HashMap<String, Autobus>>(){}.getType();
                break;
            case "Hotel":
                dataType =  new TypeToken<HashMap<String, Hotel>>(){}.getType();
                break;
            case "Lieu":
                dataType =  new TypeToken<HashMap<String, Lieu>>(){}.getType();
                break;
            case "Partenaire":
                dataType =  new TypeToken<HashMap<String, Partenaire>>(){}.getType();
                break;
            case "Sejour":
                dataType =  new TypeToken<HashMap<String, Sejour>>(){}.getType();
                break;
            case "Souhait":
                dataType =  new TypeToken<HashMap<String, Souhait>>(){}.getType();
                break;
            case "User":
                dataType =  new TypeToken<HashMap<String, User>>(){}.getType();
                break;
            case "Voiture":
                dataType =  new TypeToken<HashMap<String, Voiture>>(){}.getType();
                break;
            case "Vol":
                dataType =  new TypeToken<HashMap<String, Vol>>(){}.getType();
                break;
            default:
                System.out.print("Erreur dans la base de donnée");
                return null;
        }

        Gson gson = new Gson();

        try {
            FileInputStream fileOut = new FileInputStream("dataBase/"+type+".json");
            ObjectInputStream in = new ObjectInputStream(fileOut);
            String dataObject = (String) in.readObject();
            JsonParser parser = new JsonParser();
            JsonObject o = parser.parse(dataObject).getAsJsonObject();
            data = gson.fromJson(o,dataType);

/*
            if(dataObject.getClass().isInstance(gson.fromJson(jsonObject,data.getClass())))
                data =
            else
                System.out.print("Erreur dans la base de donnée");*/
            in.close();
            fileOut.close();

        }catch (StreamCorruptedException i){
            FileInputStream fileOut = null;
            try {
                String chaine="";
                InputStream ips= new FileInputStream("dataBase/"+type+".json");
                InputStreamReader ipsr=new InputStreamReader(ips);
                BufferedReader br=new BufferedReader(ipsr);
                String ligne;
                while ((ligne=br.readLine())!=null){
                    chaine+=ligne+"\n";
                }
                JsonParser parser = new JsonParser();
                JsonObject o = parser.parse(chaine).getAsJsonObject();
                data = gson.fromJson(o,dataType);

                ipsr.close();
                ips.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }catch(IOException | ClassNotFoundException i) {
            System.out.print("Error while reading object : " + i);
        }

        return data;
    }

    public void setData(String type,Entity entity){


        HashMap<String, Entity> data = getData(type);

        if(data.containsKey(entity.getId()))
            data.replace(entity.getId(), entity);
        else
            data.put(entity.getId(), entity);


        Gson gson = new Gson();

        try {
            FileOutputStream fileOut = new FileOutputStream("dataBase/"+type+".json");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(gson.toJson(data,data.getClass()));
            out.close();
            fileOut.close();
        }catch(IOException i) {
            System.out.print("Error while writing object : " + i);
        }

/*
        try {
            FileOutputStream fileOut = new FileOutputStream("dataBase/"+type+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(data);
            out.close();
            fileOut.close();
        }catch(IOException i) {
            System.out.print("Error while writing object : " + i);
        }*/
    }




}
