package repository;

import entity.Entity;
import entity.Lieu;
import entity.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 26/04/2016.
 */
public class LieuRepository extends Repository {


    public LieuRepository() {
        this.entity = new Doctrine().getData("Lieu");
    }

    public Lieu getOneByPays(String pays){


        for(Map.Entry<String, Entity> entry : entity.entrySet()) {
            //String key = entry.getKey();
            Lieu value = (Lieu) entry.getValue();
            if(value.getPays().equals(pays))
                return value;
        }

        return null;

    }

    public Lieu getOneByVille(String ville){


        for(Map.Entry<String, Entity> entry : entity.entrySet()) {
            //String key = entry.getKey();
            Lieu value = (Lieu) entry.getValue();
            if(value.getVille().equals(ville))
                return value;
        }

        return null;

    }





}