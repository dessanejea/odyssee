package repository;

import entity.Entity;
import entity.Lieu;
import entity.Sejour;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 26/04/2016.
 */
public class SejourRepository extends Repository {


    public SejourRepository() {
        this.entity = new Doctrine().getData("Sejour");
    }


    public Sejour getOneByVille(String ville){


        for(Map.Entry<String, Entity> entry : entity.entrySet()) {
            Sejour value = (Sejour) entry.getValue();
            if(value.getLieu().getVille().equals(ville))
                return value;
        }

        return null;

    }

    public Sejour getOneByPays(String pays){


        for(Map.Entry<String, Entity> entry : entity.entrySet()) {
            Sejour value = (Sejour) entry.getValue();
            if(value.getLieu().getPays().equals(pays))
                return value;
        }

        return null;

    }
}
