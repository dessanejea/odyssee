package repository;

import entity.Entity;
import entity.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Administrator on 12/03/2016.
 */
public class UserRepository extends Repository {


    public UserRepository() {
        this.entity = new Doctrine().getData("User");
    }

    public User getOneByMail(String mail){


        for(Map.Entry<String, Entity> entry : entity.entrySet()) {
            //String key = entry.getKey();
            User value = (User) entry.getValue();
            if(value.getMail().equals(mail))
                return value;
        }

        return null;

    }

}
