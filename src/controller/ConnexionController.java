package controller;


import entity.User;
import model.BaseModel;
import model.Observable;
import repository.Doctrine;
import view.Accueil;
import view.Base;
import view.Connexion;
import view.PanelView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created by Administrator on 11/03/2016.
 */
public class ConnexionController extends Controller {


    private String mail;


    private String password;

    private boolean valid = true;



    protected ArrayList<String> listOperateur = new ArrayList<String>();


    public ConnexionController(BaseModel obs) {
        super(obs);
    }

    public PanelView doAction(ActionEvent e, ConnexionController controller, Base window){


        User user = controller.doctrine.getUserRepository().getOneByMail(controller.getMail());
        if(((JButton)e.getSource()).getText().equals("retour")||(user!=null&&user.getPassword().equals(controller.getPassword()))){
            this.model.setUser(user);
            return new Accueil(new AccueilController(this.model),window);
        }else {
            controller.valid = false;
            return new Connexion(controller,window);
        }

    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValid() {
        return valid;
    }
}
