package controller;


import entity.User;
import model.BaseModel;
import view.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created by Administrator on 11/03/2016.
 */
public class InscriptionController extends Controller {


    private String mail;

    private String password;

    private String password2;

    private String name;

    private String firstname;


    private boolean valid = true;



    protected ArrayList<String> listOperateur = new ArrayList<String>();


    public InscriptionController(BaseModel obs) {
        super(obs);
    }

    public PanelView doAction(ActionEvent e, InscriptionController controller, Base window){





        if(((JButton)e.getSource()).getText().equals("retour")){
            return new Accueil(new AccueilController(this.model),window);
        }else{
            if(controller.password.equals(controller.password2)){
                User user = new User();
                user.setPassword(controller.password);
                user.setFirstName(controller.getFirstname());
                user.setLastName(controller.getName());
                user.setMail(controller.getMail());
                user.persist();
                return new Accueil(new AccueilController(this.model),window);
            }else{
                valid = false;
            }
        }

        return new Inscription(controller,window);


    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValid() {
        return valid;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstName(String surname) {
        this.firstname = surname;
    }
}
