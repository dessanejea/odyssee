package controller;


import entity.User;
import model.*;
import model.Observable;
import view.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.*;

/**
 * Created by Administrator on 11/03/2016.
 *
 */
public class AccueilController extends Controller {



    protected ArrayList<String> listOperateur = new ArrayList<String>();


    public AccueilController(BaseModel obs) {
        super(obs);
    }

    public PanelView doAction(ActionEvent e, Controller controller, Base window){
        switch (((JButton)e.getSource()).getText()){
            case "Inscription":
                return new Inscription(new InscriptionController(this.model),window);
            case "Connexion":
                return new Connexion(new ConnexionController(this.model),window);
            default:
                return new Accueil(new AccueilController(this.model),window);
        }

    }







}
