package controller;

import model.*;
import model.Observable;
import repository.Doctrine;
import view.Accueil;
import view.Base;
import view.PanelView;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by Jean on 11/05/2016.
 */
public abstract class Controller {



    protected BaseModel model;

    protected Doctrine doctrine = new Doctrine();

    public Controller(BaseModel o){
        model =o;
    }



    public PanelView doAction(ActionEvent e, Controller controller, Base window){

        return new Accueil(controller,window);
    }


    public BaseModel getModel() {
        return model;
    }

    public void setModel(BaseModel model) {
        this.model = model;
    }



}
