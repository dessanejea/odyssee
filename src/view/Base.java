package view;

import controller.AccueilController;
import controller.ConnexionController;
import controller.Controller;
import model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

/**
 * Created by Jean on 11/05/2016.
 */
public class Base extends View{


    private JPanel container = new JPanel();

    String[] tab_string = {"Connexion","Inscription"};
    JButton[] tab_button = new JButton[tab_string.length];






    private Controller controller;


    public Base(Controller controller) {
        super(controller);


        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.controller = controller;
        container.setPreferredSize(screenSize);
        container.setLayout(null);
        this.setContentPane(container);
        new Accueil(controller,this).initComposant(this);

        this.setVisible(true);

    }








    @Override
    public void update(Observable o, Object arg) {

    }

}
