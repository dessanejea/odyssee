package view;

import controller.ConnexionController;
import controller.Controller;
import controller.InscriptionController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Jean on 11/05/2016.
 */
public class Inscription implements PanelView {


    private InscriptionController controller;
    private Base window;


    private JTextField mail = new JTextField("mail");
    private JTextField password = new JTextField("Mot de passe");
    private JTextField password2 = new JTextField("Valider le mot de passe");
    private JTextField name = new JTextField("Nom");
    private JTextField firstname = new JTextField("Prénom");


    private JButton submit = new JButton("valider");
    private JButton back = new JButton("retour");




    public Inscription(Controller controller, Base window){
        this.controller = (InscriptionController) controller;
        this.window = window;
    }

    @Override
    public void initComposant(Base window) {

        mail.setBounds(505,500,100,100);
        password.setBounds(605,500,100,100);
        password2.setBounds(705,500,100,100);
        name.setBounds(805,500,100,100);
        firstname.setBounds(905,500,100,100);

        submit.setBounds(50,50,100,100);
        back.setBounds(150,50,100,100);



        window.getContentPane().removeAll();
        window.getContentPane().setBackground(Color.red);
        window.getContentPane().add(mail);
        window.getContentPane().add(password);
        window.getContentPane().add(submit);
        window.getContentPane().add(back);
        window.getContentPane().add(password2);
        window.getContentPane().add(name);
        window.getContentPane().add(firstname);




        if(!controller.isValid()){
            JTextArea valid = new JTextArea("T'as foiré connard");
            valid.setBounds(300,300,100,100);
            window.getContentPane().add(valid);

        }

        ButtonListener btnListener = new ButtonListener();
        submit.addActionListener(btnListener);
        back.addActionListener(btnListener);


        window.invalidate();
        window.validate();
        window.repaint();
        window.setVisible(true);


    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            controller.setMail(mail.getText());
            controller.setPassword(password.getText());
            controller.setPassword2(password2.getText());
            controller.setFirstName(firstname.getText());
            controller.setName(name.getText());



            PanelView view = controller.doAction(e,controller,window);
            view.initComposant(window);


        }
    }



}
