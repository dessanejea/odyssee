package view;

import controller.AccueilController;
import controller.ConnexionController;
import controller.Controller;
import entity.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Jean on 11/05/2016.
 */
public class Accueil implements PanelView {


    private Controller controller;
    private Base window;


    private  JButton inscription = new JButton("Inscription");
    private  JButton connexion = new JButton("Connexion");



    public Accueil(Controller controller,Base window){
        this.controller = controller;
        this.window = window;
    }

    @Override
    public void initComposant(Base window) {



        inscription.setBounds(50,50,100,100);
        connexion.setBounds(150,50,100,100);

        //JE VIDE LA FENETRE
        window.getContentPane().removeAll();


        //J'AJOUTE LES TRUCS
        window.getContentPane().add(inscription);
        window.getContentPane().add(connexion);

        //Nous utiliserons le même listener pour tous les opérateurs
        ButtonListener btnListener = new ButtonListener();
        connexion.addActionListener(btnListener);
        inscription.addActionListener(btnListener);

        User user = controller.getModel().getUser();
        if(user!=null){
            JTextArea name = new JTextArea(user.getFirstName());
            name.setBounds(10,200,100,100);

            window.getContentPane().add(name);


        }

        window.invalidate();
        window.validate();
        window.repaint();
        window.setVisible(true);


    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //controller.setState(((JButton)e.getSource()).getText());

            PanelView view = controller.doAction(e,controller,window);
            view.initComposant(window);


        }
    }



}
