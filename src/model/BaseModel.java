package model;

import entity.User;
import repository.Doctrine;
import view.View;

import java.util.*;
/**
 * Created by Jean on 11/05/2016.
 */
public class BaseModel implements Observable{


    private ArrayList<View> listObserver = new ArrayList<View>();


    private User user;


    @Override
    public void addObserver(View obs) {
        this.listObserver.add(obs);
    }



    @Override
    public void removeObserver() {
        listObserver = new ArrayList<View>();
    }

    @Override
    public void notifyObserver(String str) {
        /*if(str.matches("^0[0-9]+"))
            str = str.substring(1, str.length());

        for(View obs : listObserver)
            obs.update(str);*/
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
