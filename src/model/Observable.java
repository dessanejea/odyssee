package model;

import view.View;

/**
 * Created by Jean on 11/05/2016.
 */

public interface Observable {
    public void addObserver(View obs);
    public void removeObserver();
    public void notifyObserver(String str);
}
